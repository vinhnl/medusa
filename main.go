package main

import (
	"bufio"
	"bytes"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"
)

type (
	tunnel struct {
		Host     string
		Port     string
		Username string
		Password string
		PID      int
		Status   bool // true = Running
	}
)

var (
	tunnelPIDs []int
)

const (
	basePort int = 10000
)

func main() {
	// TODO add command parameters
	tunnels := loadTunnelConfig("ssh.txt")

	for i, v := range tunnels {
		// TODO configurate port range
		if i > 10000 {
			break
		}

		v.Port = strconv.Itoa(basePort + i)
		go createTunnel(v)
	}

	ch := make(chan os.Signal, 2)
	signal.Notify(ch, os.Interrupt, os.Kill)
	go func() {
		<-ch
		log.Printf("Waiting...\n")
		time.Sleep(10 * time.Second)
		log.Printf("Cleaning up %v open tunnel processes...\n", len(tunnelPIDs))
		for _, v := range tunnelPIDs {
			log.Printf("Kill process  %v\n", v)
			syscall.Kill(-v, syscall.SIGKILL)
		}
		log.Println("Done")
		os.Exit(1)
	}()

	select {}
}

func createTunnel(t *tunnel) {
	cmd := exec.Command("sshpass", "-p"+t.Password, "ssh", "-oStrictHostKeyChecking=no", "-oUserKnownHostsFile=/dev/null", "-oGlobalKnownHostsFile=/dev/null", "-oServerAliveInterval=180", "-D"+t.Port, "-N", "-C", t.Username+"@"+t.Host)
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	err := cmd.Start()
	if err != nil {
		log.Println("F:" + t.Username + ":" + t.Host + ":" + t.Port)
		log.Panicln(err)
	}

	t.PID = cmd.Process.Pid
	tunnelPIDs = append(tunnelPIDs, cmd.Process.Pid)

	time.AfterFunc(5*time.Second, func() {
		if verifyTunnel(t) {
			t.Status = true
			log.Println("S:" + t.Username + ":" + t.Host + ":" + t.Port)
		} else {
			t.Status = false
			log.Println("F:" + t.Username + ":" + t.Host + ":" + t.Port)
		}
	})

	err = cmd.Wait()
	if err != nil {
		// Do nothing
	}
}

func verifyTunnel(t *tunnel) bool {
	cmd := exec.Command("nc", "-z", "localhost", t.Port)
	out := &bytes.Buffer{}
	// This command return result with stderr not stdout!
	cmd.Stderr = out

	err := cmd.Run()
	if err == nil && out != nil && strings.Contains(out.String(), "succeeded") {
		return true
	}

	return false
}

func loadTunnelConfig(path string) []*tunnel {
	handler, _ := os.Open(path)
	defer handler.Close()
	scanner := bufio.NewScanner(handler)

	var rs []*tunnel
	for scanner.Scan() {
		s := strings.Split(scanner.Text(), ":")

		t := &tunnel{
			Host:     s[0],
			Username: s[1],
			Password: s[2],
		}
		rs = append(rs, t)
	}
	return rs
}
